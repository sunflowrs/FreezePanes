//
//  ViewController.swift
//  FreezePanes
//
//  Created by Cluy on 16/11/22.
//  Copyright © 2016年 Cluy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var titleTable: BasicTableView!
    @IBOutlet weak var listTable: BasicTableView!
    @IBOutlet weak var scorllerView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        scorllerView.contentSize = CGSize(width:2040,height:0)
        scorllerView.bounces = false
        titleTable.titleArr = ["统计类型","客流总量","留档客户","留档率%","购车客户数","成交率%"]
        titleTable.tag = 1001
        titleTable.reloadData()
        listTable.tag = 1002
        listTable.titleArr = ["统计类型","客流总量","留档客户","留档率%","购车客户数","成交率%"]
        listTable.listArr = [["date":"1","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"2","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"3","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"4","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"5","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"6","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"7","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"8","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"9","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"10","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"11","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"12","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"13","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"14","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"15","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"16","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"17","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"18","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"19","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"20","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"21","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"22","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"23","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"24","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"25","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"26","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"27","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"28","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"29","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"30","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"],["date":"31","total":"a","projectCount":"b","visitrate":"v","abandonCount":"r","abandonrate":"50"]]
        listTable.reloadData()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

