//
//  BasicTableView.swift
//  FreezePanes
//
//  Created by Cluy on 16/11/22.
//  Copyright © 2016年 Cluy. All rights reserved.
//

import Foundation
import UIKit
class BasicTableView:UITableView,UITableViewDelegate,UITableViewDataSource{
    var titleArr:Array<String> = []
    var listArr:Array<Dictionary<String,String>> = []
    override func awakeFromNib() {
        super.awakeFromNib()
        self.delegate = self
        self.dataSource = self
        self.separatorStyle = UITableViewCellSeparatorStyle.none
        self.showsVerticalScrollIndicator = false
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var  identifierStr :String
        if tableView.tag == 1001 {
            identifierStr = "tableViewCell"
        }else{
            identifierStr = "tableViewCell2"
        }
        
        var cell:UITableViewCell = UITableViewCell.init()
        if cell.isEqual(nil){
            cell = UITableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: identifierStr)
        }
        if indexPath.row%2 == 0{
            cell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }else{
            cell.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
        if tableView.tag == 1001 {
            cell.textLabel!.text = String(describing: titleArr[ indexPath.row])
            cell.textLabel?.textAlignment = NSTextAlignment.center
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            if indexPath.row == 0 {
                cell.textLabel?.textColor = UIColor (colorLiteralRed: 16/255.0, green: 86/255.0, blue: 186/255.0, alpha: 1.0)
            }else{
                cell.textLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
            
        }else{
            for (idx,value) in  listArr.enumerated() {
                let label:UILabel = UILabel.init(frame: CGRect(x:idx*60,y:0,width:60,height: Int(cell.frame.size.height)))
                label.textAlignment = NSTextAlignment.center
                label.font = UIFont.boldSystemFont(ofSize: 14.0)
                if indexPath.row == 0 {
                    label.textColor = UIColor (colorLiteralRed: 16/255.0, green: 86/255.0, blue: 186/255.0, alpha: 1.0)
                }else{
                    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                }
                cell.contentView .addSubview(label)
                if idx == listArr.count-1{
                    cell.frame = CGRect(x:cell.frame.origin.x,y:cell.frame.origin.y,width:label.frame.origin.x+label.frame.size.width,height:cell.frame.size.height)
                }
                switch (indexPath.row) {
                case 0:
                    label.text = value["date"]! + "日"
                    
                case 1:
                    label.text = value["total"]
                    
                case 2:
                    label.text = value["projectCount"]
                    
                case 3:
                    label.text = value["visitrate"]
                    
                    
                case 4:
                    label.text = value["abandonCount"]
                    
                    
                case 5:
                    label.text = value["abandonrate"]
                    
                default:
                    label.text = ""
                }
            }
        }
            return cell;
            
        }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 40;
        }
        return 50;
    }

}
